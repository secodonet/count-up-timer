function CountUpTimer(startDate, onIncrementCallback, endDate) {
    var daysPassed = 0;
    var hoursPassed = 0;
    var minutesPassed = 0;
    var secondsPassed = 0;

    var startDate = startDate || new Date();
    var now = endDate || new Date();
    var diff = new Date(now.getTime() - startDate.getTime());

    var shouldIncrement = true;

    this.stop = function () {
        shouldIncrement = false;
    }

    //console.log(diff);

    function setTimePassed() {
        daysPassed = Math.round(diff/1000/60/60/24); //diff.getDate() - 1;
        hoursPassed = diff.getHours() - 1;
        minutesPassed = diff.getMinutes();
        secondsPassed = diff.getSeconds();

        if (hoursPassed < 0) {
            daysPassed--; //  minus one day
            hoursPassed += 24; // plus 24 hours from taken day

        }
    }

    setTimePassed();

    function increment() {
        secondsPassed++;
        if (secondsPassed > 59) {
            secondsPassed = 0
            minutesPassed++;

            if (minutesPassed > 59) {
                minutesPassed = 0;
                hoursPassed++;

                if (hoursPassed > 23) {
                    hoursPassed = 0;
                    daysPassed++;
                }
            }
        }

        onIncrementCallback({seconds: secondsPassed, minutes: minutesPassed, hours : hoursPassed, days : daysPassed});

        if (shouldIncrement) {
            setTimeout(increment, 1000);
        }
    }

    increment();

    // console.log(startDate);
    // console.log(now);
    // console.log("days: " + daysPassed);
    // console.log("hours: " + hoursPassed);
    // console.log("minutes: " + minutesPassed);
    // console.log("seconds: " + secondsPassed);
    // console.log("----------");
}

// Export node module.
if ( typeof module !== 'undefined' && module.hasOwnProperty('exports') ) {
    module.exports = CountUpTimer;
}