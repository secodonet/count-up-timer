describe("count-up-timer tests", function() {
    var CountUpTimer = require('../../src/lib/CountUpTimer');

    it("contains spec with an expectation", function() {
        var testInterval;
        var timer = new CountUpTimer(new Date(2017,1,3,  12,0,0), function (interval) {
            testInterval = interval;
        }, new Date(2017,1,3,   13,20,2));

        timer.stop();

        expect(testInterval.seconds).toBe(3);
        expect(testInterval.minutes).toBe(20);
        expect(testInterval.hours).toBe(1);
        expect(testInterval.days).toBe(0);
    });

});

